# Python program to rename all file 
# names in your directory  
import os

os.chdir('/home/acta/Desktop/png') 
print(os.getcwd()) 
COUNT = 551
  
# Function to increment count  
# to make the files sorted. 
def increment(): 
    global COUNT 
    COUNT = COUNT + 1
  
  
for f in os.listdir(): 
    f_name, f_ext = os.path.splitext(f) 
    f_name = "frame" + str(COUNT)
    print(f_name)
    increment() 
  
    new_name = '{}{}'.format(f_name, f_ext) 
    os.rename(f, new_name)

''' 
os.chdir('/home/acta/Desktop/xml') 
print(os.getcwd()) 
COUNT = 1
 

# Function to increment count  
# to make the files sorted. 
def increment(): 
    global COUNT 
    COUNT = COUNT + 1
  
  
for f in os.listdir(): 
    f_name, f_ext = os.path.splitext(f) 
    f_name = "frame" + str(COUNT)
    print(f_name)
    increment() 
  
    new_name = '{} {}'.format(f_name, f_ext) 
    os.rename(f, new_name)
''' 
