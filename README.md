# Neural Network Training Algorithm: ArUco Drone Bebop

This algorithm is related to the training of convolutional neural networks for identification of ArUco landing markers. The package was developed based on the [AMP Tech -](https://inteligencia.tech/2019/02/24/deteccion-de-objetos-con-tensorflow/)

[YouTube (Parte 1) - AMP Tech](https://www.youtube.com/watch?v=SJRP0IRfPj0)

[YouTube (Parte 2) - AMP Tech](https://www.youtube.com/watch?v=EKe05rMG-Ww&t=321s)

Tensorflow Models - [Config Files](https://github.com/tensorflow/models/tree/master/research/object_detection/samples/configs)

Difference between MobileNet v1 x v2 - [MobileNet Version 2](https://machinethink.net/blog/mobilenet-v2/)

Training Custom Object Detector - Tensorflow Object Detection API Tutorial - [SSD Mobilenet V1](https://pythonprogramming.net/training-custom-objects-tensorflow-object-detection-api-tutorial/)

Tensorflow Object Detector with ROS - [Github ROS](https://github.com/alanprodam/hybrid_detection/tree/alan-working)

In this tutorial you will see how to make an object detector using the Tensor Flow object detection API. To begin with, it is important to know the distinction between an object classifier and an object detector.

## Image classification: 

Algorithm which is responsible for classifying ALL an image within a category. For example, we give him a picture of a car and he only mentions that the predominant object in the photo is a car.

## Object detection: 

Algorithm which is responsible for detecting various elements within an image and classify them. For example, we give you an image that contains a transit stop and identifies us in the image there is a traffic light, cars, traffic signs, trees, etc.
     

Version | Linux | Operating System 
------------ | ------------- | ------------
1.15.0 | Tensorflow-GPU | Ubuntu 16.04 LTS (Xenial)
1.15.0 | Tensorflow | Ubuntu 16.04 LTS (Xenial)
1.12.0 | Tensorboard | Ubuntu 16.04 LTS (Xenial)


## Install dependencies and follow the installation instructions.

- [x] Python 3.6.7
- [x] Tensorflow 
- [x] Numpy
- [x] Pandas
- [x] Matplotlib
- [x] Tarjeta de gráficos (Recomendada para poder hacer un entrenamiento de manera rápida, aunque es posible hacerlo sin GPU, puede llegar a tardar horas o días sin una tarjeta de gráficos NVIDIA


## Steps to replicate the algorithm: Object Detection with Tensorflow

### The first step

```
sudo apt-get update
sudo apt-get upgrade
```

1. Install Python libraries

```
sudo apt-get install python-dev python-pip python3-dev python3-pip
sudo -H pip2 install -U pip numpy
sudo -H pip3 install -U pip numpy
```

Upgrade pip

```
sudo -H pip2 install --upgrade pip
sudo -H pip3 install --upgrade pip
```

2. Install Tensorflow

```
pip install launchpadlib
# For CPU
sudo pip install tensorflow==1.15.0
# For GPU
sudo pip install tensorflow-gpu==1.15.0

sudo pip install --upgrade tensorflow
# How version of TensorFlow is installed
pip3 list | grep tensorflow
```

> **Note:** Be careful with the version of the Tensorflow, if you use 2.0, it may generate an error.

3. Install pandas

```
sudo pip install pandas
```

4. Install numpy and scipy

```
sudo apt install python-numpy
sudo apt install python3-scipy
```

5. Install matplotlib

```
sudo apt-get install python3-matplotlib
```

### Train and Test

### Create Tags (mscoco_label_map.pbtxt)

In this file `(configuration/mscoco_label_map.pbtxt)` it will tell our algorithm which are the labels on which we will train it. The name we put on the labels must be the same as we use in the labelImg tool (including uppercase and spaces). Basically this file has a series of elements ‘item’ with its respective identifier ‘id’ and class name ‘name’.

Here is an example, this changes according to the number of elements you want to learn to detect.

```
item {
  name: 'landmark'
  id: 1
  display_name: "Landmark"
}
```

### Create Tags (labels.txt)

This file is similar to the past but much simpler in `(configuration/label.txt)`, it is only a list of the elements that we want to detect, being the first element (in the first line) always the null value. Here is an example.

```
null
landmark
```

#### Convert images to TFRecord

When running locally, the tensorflow/models/ and slim directories should be appended to PYTHONPATH. This can be done by running the following from tensorflow/models/:

```
cd ~/rcnn_landmark/deteccion
export PYTHONPATH=$PYTHONPATH:`pwd`:`pwd`/slim
```

> **Note:** This command needs to run from every new terminal you start. If you wish to avoid running this manually, you can add it as a new line to the end of your `~/.bashrc` file.

In this step, you should already have all of your images tagged by the `LabelImg` program inside the `img` folder. Initially you should pass about 80% of the label images created in `img` to the `img_training` folder. 

```
deteccion_objetos_atual
   |
   |---> img
   |---> img_test
   |---> img_training
	 ...
   |---> CSV
   |---> TFRecords
     ...
   |---> configuracion
   |---> model
     ...
   |---> object_detection
   |---> output
   |---> slim
   
```
Next, you should pass about 20% of the label images created in `img` to the `img_test` folder. Only then, you will be able to start training.

```
python3 xml_a_csv.py --inputs=img_test --output=mscoco_val

python3 xml_a_csv.py --inputs=img_training --output=mscoco_train

python3 csv_a_tf.py --csv_input=CSV/mscoco_val.csv --output_path=TFRecords/mscoco_val.record --images=img

python3 csv_a_tf.py --csv_input=CSV/mscoco_train.csv --output_path=TFRecords/mscoco_train.record --images=img
```

> **Note:** It is important to verify that the created files (.csv and .record) were created correctly.

#### Train

First you should clear the 'train', 'frozen_model' and '/ output / img_pruebas' folder.

```
python3 object_detection/train.py \
	--logtostderr \
	--train_dir=train \
	--pipeline_config_path=model/ssd_mobilenet_v2.config
```

From `rcnn_landmark/deteccion`, via terminal, you start TensorBoard with:

```
tensorboard --logdir='train'
```

> **Note:** Be careful with the version of the Tensorboard, maybe you may install `sudo pip install tensorboard==1.12`.


This runs on 127.0.0.1:6006 (visit in your browser)

#### Freeze the Trained Model

You have to change the number of 'model.ckpt-xxxx' in the parameter `--trained_checkpoint_prefix`, and put yours that was better for you. It is depends of interactions that you obtained in the train.

```
python3 object_detection/export_inference_graph.py --input_type image_tensor --pipeline_config_path model/ssd_mobilenet_v1.config --trained_checkpoint_prefix train/model.ckpt-1050 --output_directory modelo_congelado
```

#### Prediction

```
python3 object_detection/object_detection_runner.py
```

### Installation LabelImg

LabelImg is a graphical image annotation tool: [LabelImg](https://github.com/tzutalin/labelImg)

It is written in Python and uses Qt for its graphical interface.

Annotations are saved as XML files in PASCAL VOC format, the format used by ImageNet. Besides, it also supports YOLO format.

#### Build from source
Linux/Ubuntu/Mac requires at least Python 2.6 and has been tested with PyQt 4.8. However, Python 3 or above and PyQt5 are strongly recommended.


#### Ubuntu Linux

Python 3 + Qt5 (Recommended)

```
git clone git@github.com:tzutalin/labelImg.git
cd labelImg/
sudo apt-get install pyqt5-dev-tools
sudo pip3 install -r requirements/requirements-linux-python3.txt
make qt5py3
python3 labelImg.py
python3 labelImg.py [IMAGE_PATH] [PRE-DEFINED CLASS FILE]
```

#### Test camera-RCNN

```
roslaunch hybrid_detection drone_cam_detector.launch
```

Enter in the link [Train](https://github.com/alanprodam/tensorflow_object_detector/tree/modelSDDMobilenet)

```
source ~/tensorflow/bin/activate
```

Using CAM-USB.

```
roslaunch hybrid_detection usb_cam_detector.launch
```
